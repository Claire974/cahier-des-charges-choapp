# Analyse fonctionnelle

*  🔖 **Identification**
*  🔖 **Critères**

___

## 📑 Identification

> Il s’agit de mettre en évidence les fonctions du produit étudié.

Le produit est considéré comme une «boite noire» et ne fait pas partie de l'analyse. Cependant les fonctions qui sont produites par cette «boite noire» doivent être minutieusement étudiées : il s'agit d'en faire l'inventaire, de les décrire et de les évaluer.

### 🏷️ **La fonctions**

> Une fonction se compose d’un verbe ou d’un groupe verbal caractérisant l’action, et de compléments représentant les éléments du milieu extérieur concernés par la fonction.

Le sujet de la phrase n’apparaît pas, mais il renvoie toujours au produit. La définition d’une fonction est la suivante: «Action d’un produit ou de l’un de ses constituants exprimée exclusivement en termes de finalité».

Outre cette définition formelle, certaines règles d’usage sont à respecter :

* La formulation selon une forme passive ou une forme négative sont à éviter

* La formulation de la fonction doit être indépendante des solutions susceptibles de la réaliser

* La formulation doit être la plus concise et la plus claire possible

### 🏷️ **Les types de fonctions**

* Fonction principale

C’est la fonction qui satisfait le besoin. Elle assure la prestation du service rendu. C'est la raison pour laquelle le produit a été créé. Une fonction principale peut être répartie en plusieurs fonctions élémentaires (action attendue d'un produit pour répondre à un élément du besoin, traduisant la raison d’être d’un sous-système du produit).

* Fonction contrainte

> « Une contrainte c'est une limitation à la liberté de choix du concepteur-réalisateur d’un produit ».

Les contraintes participent à définir le besoin en recensant les conditions qui doivent être impérativement vérifiées par le produit, mais qui ne sont pas sa raison d’être. Ces conditions peuvent être liées au marché, à la stratégie de l’entreprise, aux environnements à considérer, à la technologie ou, bien sûr, à la réglementation.

* Fonction complémentaire

Fonction qui facilite, améliore, ou complète le service rendu. Ce type de fonction ne résulte pas de la demande explicite du client, et n’est pas non plus une contrainte. Il s'agit de proposer au client des améliorations pour son produit et la qualité.

### 🏷️ **Exemple**

> La tondeuse à gazon

Principale:
* Réduire la hauteur de l’herbe

Contraintes:
* S’adapter au terrain et à la végétation.
* Respecter l’environnement (écologie et sonorité).
* Faciliter l’utilisation.
* Assurer la concurrentialité.
* Présenter un encombrement minimum.
* Respecter les normes de sécurité.
* Résister aux intempéries.
* Capter de l’énergie et l’utiliser.

Complémentaires:

* Ramasser l’herbe.
* Fonctionner sans l’intervention.

___

👨🏻‍💻 Manipulation

Au format tableau: identifiez les différentes fonctions sur la thématique présentée en fonction de leur nature.

___

## 📑 Critères

Ils caractérisent qualitativement la fonction, ils possèdent un niveau qui doit permettre de quantifier le critère.

### 🏷️ **Critère**

Chaque fonction possède un ou plusieurs critères d'appréciation. Le critère d’appréciation d’une fonction est le caractère retenu pour apprécier la manière dont une
fonction est remplie ou une contrainte respectée.

Par exemple:

* Le poid
* L'autonomie
* Le temps démarrage

### 🏷️ **Niveau**

Le niveau d’un critère d’appréciation est la grandeur repérée dans l’échelle adoptée pour un critère
d’appréciation d’une fonction. Cette grandeur peut être celle recherchée en tant qu’objectif ou celle
atteinte pour une solution proposée.

Par exemple:

* Inférieur à 15 K
* Supérieur à 1h
* Inférieur à 1 minute

___

👨🏻‍💻 Manipulation

Complétez le tableau précédent en associant aux fonctions les critères et niveaux.

# La rédaction

*  🔖 **La rédaction**
*  🔖 **La validation**

___

## 📑 La rédaction

Comme exposé précédement, le cacheir des charges n'expose pas uniquement les fonctionnalitées mais également son contexte et propose une conclusion.

### 🏷️ **Étude d'opportunité**

Cela consiste à étudier le contexte du projet, à déterminer les besoins généraux de la maîtrise d'ouvrage et à vérifier si ceux-ci correspondent bien aux attentes des utilisateurs finaux et aux évolutions probables à venir. Cela permet de décider de la viabilité du projet. Cette étape se conclut par la livraison d'une « note de cadrage » qui établit officiellement l'intention de projet.

### 🏷️ **Étude de faisabilité**

Cette étape consiste à valider la capacité à réaliser le projet suivant différents critères:

* Il est économiquement viable, car le retour sur investissement est supérieur à l'investissement demandé.
* Les technologies nécessaires sont disponibles.
* L'organisation peut s'adapter à ce changement lié à l'acceptation de la charge de travail.
* Les contraintes réglementaires le permettent.
* La durée d'exécution du projet est acceptable.

Ceci mène aux études de scénario, qui envisagent les risques pouvant menacer le projet et présentent un bilan prévisionnel

### 🏷️ **Analyse fonctionnelle**

Nous avons détaillé cette section dans le chapitre précédent.

### 🏷️ **Analyse de la valeur**

L'analyse de la valeur s'appuie aussi sur les solutions à apporter au problème et leurs coûts, pour les évaluer.

De par sa nature contractuelle, et pour prévenir des dysfonctionnements futurs, il est très important que chacune des parties participe activement à la rédaction du cahier des charges fonctionnel.

___

👨🏻‍💻 Manipulation

Complétez le document précédement initié en renseignant les différentes sections manquantes exposées. Il est possiblze de récupérer des modèles préfaits de chaier des charges!